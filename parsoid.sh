#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml --force

if [[ "${1}" != "" ]]; then
  ansible-playbook parsoid.yml -i hosts.yml --tags "${1}" -v
else
  ansible-playbook parsoid.yml -i hosts.yml  -v
fi
