#!/usr/bin/env bash


if [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force matomo && \
    ansible-playbook matomo_upgrade.yml -i hosts.yml --limit "${1}" -vv
else
  ansible-galaxy install -r requirements.yml --force matomo && \
    ansible-playbook matomo_upgrade.yml -i hosts.yml -vv
fi
