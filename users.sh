#!/usr/bin/env bash

set -e -o pipefail

if [[ "${2}" != "" ]]; then
  # ansible-galaxy install -r requirements.yml --force users && \
    ansible-playbook users.yml -i hosts.yml --tags "${1}" --limit "${2}" --diff
elif [[ "${1}" != "" ]]; then
  # ansible-galaxy install -r requirements.yml --force users && \
    ansible-playbook users.yml -i hosts.yml --tags "${1}" --diff
else
  # ansible-galaxy install -r requirements.yml --force users && \
    ansible-playbook users.yml -i hosts.yml --diff
fi
