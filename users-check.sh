#!/usr/bin/env bash

if [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook users_check.yml -v --limit "${1}"
else
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook users_check.yml -v 
fi

