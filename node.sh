#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml --force nodejs && \
ansible-playbook wsh.yml -t nodejs,chroot
