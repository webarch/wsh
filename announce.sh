#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml --force

if [[ "${2}" != "" ]]; then
  ansible-playbook announce.yml -i hosts.yml --limit "${1}" -v
else
  ansible-playbook announce.yml -i hosts.yml -v
fi
