#!/usr/bin/env bash
  
if [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force upgrade && \
    ansible-playbook upgrade.yml -i hosts.yml -v --limit "${1}"
else
  ansible-galaxy install -r requirements.yml --force upgrade && \
    ansible-playbook upgrade.yml -i hosts.yml -v
fi
