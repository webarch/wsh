# Webarchitects Secure Hosting

Debian Buster based, third generation of the [Webarch Secure Hosting](https://docs.webarch.net/wiki/Webarch_Secure_Hosting) servers, see the [ideas thread on the members forum](https://members.webarchitects.coop/t/the-next-iteration-of-our-shared-web-hosting-service/89).

This repo is set to manage a development server, `wsh.webarchitects.org.uk`, if you would like to use this repo to manage server(s) then copy it and then edit [hosts.yml](hosts.yml) and move and edit [host_vars/wsh.webarchitects.org.uk.yml](host_vars/wsh.webarchitects.org.uk.yml).

For guidance for adding accounts see the [host_vars/README.md](host_vars/README.md) file.

The [user.sh](users.sh) script will be automatically run via CI if a git commit *only* updates the [host_vars/wsh.webarchitects.org.uk.yml](host_vars/wsh.webarchitects.org.uk.yml) file, in other cases manual actions are needed.

The current stable branch is `v0.9.2`.

## Running the playbooks

Before manally using `ansible-playbook`, rather than the Bash wrapper scripts, you can ensure that the Ansible Galaxy roles are all up to date using:

```bash
ansible-galaxy install -r requirements.yml --force
```

### Only run checks on users

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=check"
```

### Only update the firewall rules

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=firewall"
```

### Update only changed users

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=changed" # this is the default
```

### Update all users

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=all"
```

### Update users PHP-FPM pool.d files

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=phpfpm"
```

### Update users Apache sites-available files

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=apache"
```

### Update users SSH public keys

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=sshkeys"
```

### Updated users in additional groups

Update all users which have `users_groups` defined:

```bash
ansible-playbook users.yml --extra-vars "users_update_strategy=groups"
```

### Update everything

```bash
ansible-playbook wsh.yml --extra-vars "users_update_strategy=all"
```

## Disk Partitions

When generating a virtual server use a `/etc/xen-tools/partitions.d/wshbuster` disk configuration like this:

```ini
[root]
size=6G
type=ext4
mountpoint=/
options=defaults,errors=remount-ro

[var]
size=2G
type=ext4
mountpoint=/var
options=defaults,errors=remount-ro

[swap]
size=2G
type=swap

[home]
size=60G
type=ext4
mountpoint=/home
options=nodev,nosuid,quota,usrjquota=aquota.user,jqfmt=vfsv0

[var-lib-mysql]
size=10G
type=ext4
mountpoint=/var/lib/mysql
options=nodev,nosuid

[chroot]
size=3G
type=ext4
mountpoint=/chroot
options=nosuid
```

## SSH for CI

For GitLab CI for automatically updating user accounts generated a `SSH_KNOWN_HOSTS` GitLab CI env var:

```bash
ssh-keyscan -H wsh.webarchitects.org.uk > /tmp/known_hosts`
```

Generate a key pair:

```bash
ssh-keygen -t ecdsa -f /tmp/wsh_ecdsa
  Generating public/private ecdsa key pair.
  Enter passphrase (empty for no passphrase):
  Enter same passphrase again:
  Your identification has been saved in /tmp/wsh_ecdsa.
  Your public key has been saved in /tmp/wsh_ecdsa.pub.
```

Then add the `/tmp/wsh_ecdsa.pub` file to `/root/.ssh/authorized_keys` on the server, prefixed with `from="81.95.52.70"` in order that it can only be used from `runner.git.coop`,  and save the contents of the private key to a `SSH_PRIVATE_KEY` GitLab CI env var.
