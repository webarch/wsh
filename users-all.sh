#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml --force

if [[ "${2}" != "" ]]; then
  ansible-playbook users_all.yml -i hosts.yml --tags "${1}" --limit "${2}"
elif [[ "${1}" != "" ]]; then
  ansible-playbook users_all.yml -i hosts.yml --tags "${1}"
else
  ansible-playbook users_all.yml -i hosts.yml
fi
