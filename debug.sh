#!/usr/bin/env bash

if [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force debug && \
    ansible-playbook debug.yml -i hosts.yml --limit "${1}" -v
else
  ansible-galaxy install -r requirements.yml --force debug && \
    ansible-playbook debug.yml -i hosts.yml  -v
fi
