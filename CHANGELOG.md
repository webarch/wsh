# Changelog for Webarchitects Secure Hosting

## 2020-02-01

- Stable version v0.9.2 lots of updates, work needs on CI tasks before v1.0
- [apache](https://git.coop/webarch/apache) version 2.4.41 installed from Buster backports for TLSv1.3
- [drupal](https://git.coop/webarch/drupal) added
- [flarum](https://git.coop/webarch/flarum) added
- [nextcloud](https://git.coop/webarch/nextcloud) added
- [kimai](https://git.coop/webarch/kimai) added
- [bower](https://git.coop/webarch/bower) removed due to [this issue](https://github.com/yarnpkg/yarn/issues/7814#issuecomment-581065055)

## 2019-10-11

- `users_matomo_delete_users` option added to [disable the deletion of Matomo accounts](https://git.coop/webarch/wsh/issues/7)

## 2019-10-10

- `yarn` is needed for phpMyAdmin install
- Failed to track down or replicate #13
- `CHANGELOG.md` added
